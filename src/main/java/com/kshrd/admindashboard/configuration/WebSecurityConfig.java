package com.kshrd.admindashboard.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private SimpleAuthenticationSuccessHandler successHandler;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/admin/login")
                .successHandler(successHandler())
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/admin/logout")
                .logoutSuccessUrl("/admin/login")
                .permitAll()
                .and().csrf().disable();
    }

@Bean
InMemoryUserDetailsManager userDetailsManager() {

    User.UserBuilder builder = User.withDefaultPasswordEncoder();

    UserDetails dara = builder.username("dara").password("dara123").roles("USER").build();
    UserDetails makara = builder.username("makara").password("makara123").roles("ADMIN").build();
    UserDetails kanha = builder.username("kanha").password("kanha123").roles("EDITOR").build();
    UserDetails reaksmey = builder.username("reaksmey").password("reaksmey123").roles("REVIEWER").build();

    return new InMemoryUserDetailsManager(dara, makara,kanha,reaksmey);
}
    @Bean
    public SimpleAuthenticationSuccessHandler successHandler() {
        return new SimpleAuthenticationSuccessHandler();
    }


}
