package com.kshrd.admindashboard.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/admin/login").setViewName("admin/login/login-form");
        registry.addViewController("/admin/dashboard").setViewName("static-content/content");
        registry.addViewController("/admin/article").setViewName("admin/article/article");
        registry.addViewController("/admin/review-articles").setViewName("admin/article/article-list");
        registry.addViewController("/admin/logout").setViewName("admin/article/article-list");
    }
}
