package com.kshrd.admindashboard.controller;

import com.kshrd.admindashboard.security.IsAdmin;
import com.kshrd.admindashboard.security.IsEditor;
import com.kshrd.admindashboard.security.IsReviewer;
import com.kshrd.admindashboard.model.Article;
import com.kshrd.admindashboard.repository.ArticleRepository;
import com.kshrd.admindashboard.security.IsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@IsUser
@Controller
public class AdminController {
    ArticleRepository articleRepository;
    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }
    @RequestMapping(value = "admin/article", method = RequestMethod.POST)
    @IsEditor
    public String addArticle(@ModelAttribute Article article){
        System.out.println(article.toString());
        articleRepository.save(article);
        return "redirect:/admin/review-articles";
    }
    @GetMapping("/admin/dashboard")
    @IsAdmin
    public String viewDashboard(){
        return "static-content/content";
    }

    @RequestMapping(value = "admin/article", method = RequestMethod.GET)
    @IsEditor
    public String viewArticleInsert(@ModelAttribute Article article, ModelMap modelMap){
        modelMap.addAttribute("article", article);
        return "admin/article/article";
    }
    @GetMapping("/admin/review-articles")
    @IsReviewer
    public String viewArticle(@ModelAttribute Article article, ModelMap modelMap){
        List<Article> articles = articleRepository.findAll();
        modelMap.addAttribute("articles", articles);
        return "admin/article/article-list";
    }
}
